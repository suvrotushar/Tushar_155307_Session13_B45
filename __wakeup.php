<?php

class person
{

    public $name = "Default Name";
    public $address = "Default Addres";
    public $phone = "Default Phone Number";

    public function doSomething()
    {
        echo "Wake up. <br>";
    }

    public function __wakeup()
    {
        $this->doSomething();
    }
}

$newPerson = new person();
$myVar = serialize($newPerson);

$newObj = unserialize($myVar);
var_dump($newObj);
